C-IDE Demo项目
=======

C-IDE是CODE提供的在线编程平台。
现阶段支持Python, Ruby, Javascript (Node.js), C, C++, Java的单文件直接执行。其它类型语言或项目可以通过terminal使用命令行进行编译及执行。


本项目提供了以下可运行脚本供参考：：

- hello_server.py Python脚本，在运行环境内的8080端口上启动一个http服务器
- helloworld.js   Node.js脚本，打印"hello world"
- helloworld.py   Python脚本，打印"hello world"
- helloworld.rb   Ruby脚本，打印"hello world"
- helloworld.c    C语言实现，打印"Hello world"
- helloworld.cpp  C++语言实现，打印"Hello World"
- HelloWorldApp.java Java语言实现，打印"Hello World"


运行
-----------
C-IDE为每个项目提供了运行环境。如果需要外部访问，需绑定到0.0.0.0 地址的8080端口上，系统将自动生成一个外部可访问的地址。

以hello_server.py为例，在点击运行按钮后，命令行提示：

你的Web应用的外部访问地址： http://xxxx.run.c-ide.csdn.net

外部就可用浏览器访问xxxx.run.c-ide.csdn.net来进行访问了

限制
------------
- 每个工作环境的内存为256M，单CPU，存储容量为2G。
- 工作环境在无访问后30分钟将会被挂起，在工作环境里运行的应用也会挂起，外部将无法访问。

我们提供这样一个环境，主要是为了方便开发者调试和测试运行。请不要运行大量消耗CPU或流量的任务，如发现有任务影响到系统运行，我们将不通知用户直接停止。

欢迎提供反馈意见帮助我们改进。
